PROJECT  **PRICE HISTORY** IN OAK FUSION   

**System design**

An application that collects products prices from various online stores, which allows you to compare the technical parameters of the compared devices. 

We aim to focus on polish stores with enormous range of _smartphones_ and with their distinction, on such attributes like ex. color, built-in memory etc.

Desired **user** is a person or organisation who wanna be up to date with:

- best prices
- follow weekly price changes of tracked devices
- models that become decommissioned
- find any hidden patterns in particular price brand strategy

**Monetization**

- Providing a platform to find the most relevant smartphone model (available only to subscribers)
- Percentage of smartphone purchase from the landing page
- Advertising on the site
- One-time, limited-time access for a nominal fee (or free access for a longer period, provided that you consent to the automatic debiting of the fee from the card after this period)
- recommending additional accessories for phones (headphones, USB, case, other gadgets)



Throughout the cycle of **11 Agile sprints** We have built end - to - end data pipeline and architecture design. 

**System consist of:**

-  web-scraping
-  storage layer
-  docker containers
-  orchiestration populate datawarehouse dimensions and data cleaning operations
-  build intuitive dashboard 

With daily increase of hundrends of new products, We present situation of smartphones prices in polish online stores


**TECHNOLOGY STACK**

![](config/Tech_stack.png)

**ETL PIPELINE DESCRIPTION**

![](config/ETL.png)

**DATAWAREHOUSE ARCHITECTURE**

![](config/SCHEMAT_DWH.png)

**WEEKLY HEATMAP**
Thanks to red-to-green spectrum colours, It's much easier to see how "crazy" the week was

![](db/price_history_Dashboard/heatmapa.png)

**ONE PRODUCT DETAILS**

![](db/price_history_Dashboard/ProductDetails.png)

**DATASET MONITORING**

Showing weekly number of products that have been loaded into our database 

![](db/price_history_Dashboard/DatasetMonitoring.png)

**TOP BEST PRODUCT ITEMS**

![](db/price_history_Dashboard/TopItems.png)

**TOP WORST PRODUCT ITEMS**

![](db/price_history_Dashboard/TopIncrease.png)

**DECOMISSED PRODUCTS**
In case of product withdrawal from shop's offer, it's essential to know about the last available date

![](db/price_history_Dashboard/DecomissedProducts.png)

**PRODUCT PRICE HISTORY**

![](db/price_history_Dashboard/ProductPriceHistory.png)

