# this python script and "WebScrap" folder need to be in the same directory

from distutils.log import error
import psycopg2
import os
import csv

# loading 
previous_loaded_files =[]
try:
	with open('previous_loaded_files.csv', newline='') as f:
		reader = csv.reader(f)
		previous_loaded_files = list(reader)
		previous_loaded_files = [item for sublist in previous_loaded_files for item in sublist]
except:
	print("file doesn't find")


conn = psycopg2.connect("host=localhost dbname=postgres user=postgres password=5432")
cur = conn.cursor()

all_existing_files = []
for root, dirs, files in os.walk("WebScrap"):
	for file in files:
		all_existing_files.append(root + "\\" + file)


for file in all_existing_files:
	if file not in previous_loaded_files:
		with open(file, 'r', encoding='utf-8') as f:
			try:
				cur.copy_expert("""COPY  price_history.smartphones_details_daily_stg(
				smartphone_brand,
				smartphone_model,
				operating_system_name,
				operating_system_version,
				ram_memory_gb,
				ram_memory_mb,
				built_in_memory_gb,
				screen_size_diagonal_full_scraped_txt,
				number_of_screens,
				main_screen_size_inch,
				scnd_screen_size_inch,
				price,
				camera_back_full_scraped_txt,
				camera_back_mpx,
				camera_back_number_of_screens,
				camera_front_full_scraped_txt,
				camera_front_mpx,
				camera_front_number_of_screens,
				battery_capacity,
				color,
				main_screen_resolution_mpx,
				scd_screen_resolution_mpx,
				source_smarthpone_url,
				scraping_date,
				dealer_name
				) FROM stdin with csv""", f)
				conn.commit()
			except:
				print("error")
cur.close()
conn.close()


with open('previous_loaded_files.csv', "a+", newline='') as f:
	writer = csv.writer(f)
	for file in all_existing_files:
		try:
			if file not in previous_loaded_files:
				writer.writerow([file])
		except:
			writer.writerow([file])