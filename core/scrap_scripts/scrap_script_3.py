print(" -------------- BEGIN OF SCRIPT 3 ---------------------- ")

import re
import sys
import os
from bs4 import BeautifulSoup
from requests import get
from regex import *
import csv
from datetime import datetime

file_path = "WebScrap/"+str(datetime.now().year)+"/"+str(datetime.now().month)+"/"+str(datetime.now().day)

ct = datetime.now()
file_name = str(ct).split()[0].replace("-", "_") + "_rtv_euro_agd" + '.csv'

#file = open(os.path.join(file_path,file_name), 'w', encoding="utf-8", newline='')
#writer = csv.writer(file)

mainURL = 'https://www.euro.com.pl/telefony-komorkowe/'

def dataCleaning(dicten):
    dicten["Przekątna wyświetlacza"] = dicten["Przekątna wyświetlacza"].split()[0]
    dicten["Rozdzielczość ekranu"] = dicten["Rozdzielczość ekranu"].split('pikseli')[0]
    dicten["System operacyjny"] = dicten["System operacyjny"].replace('producenta', 'NAN')
    dicten["Pamięć RAM"] = dicten["Pamięć RAM"].replace('poniżej', '')
    return dicten

smartphones_list = []
def getInside(url, name, price):
    URL = 'https://www.euro.com.pl/telefony-komorkowe/' + url + '.bhtml'

    keyList = {"Pojemność baterii", "System operacyjny", "Przekątna wyświetlacza", "Rozdzielczość ekranu", "Pamięć RAM",
               "Pamięć wbudowana", "Pojemność baterii", "Aparat tylny", "Wyświetlacz",
               "Kolor obudowy", "Aparat przedni"}
    maindict = {"nazwa": name, "System operacyjny": "NAN", "Pamięć RAM": "NAN", "Pamięć wbudowana": "NAN",
                "Przekątna wyświetlacza": "NAN", "Cena": price, "Aparat tylny": "NAN", "Aparat przedni": "NAN",
                "Pojemność baterii": "NAN", "Kolor obudowy": "NAN", "Rozdzielczość ekranu": "NAN", "link": URL}

    page = get(URL)
    bs = BeautifulSoup(page.content, "html.parser")

    for offer in bs.find_all('td'):
        if offer.text.strip() in maindict.keys():
            b_tag = offer
            next_td_tag = b_tag.findNext('td')
            maindict[offer.text.strip()] = next_td_tag.text.strip()

    #writer.writerow(dataCleaning(maindict).values())
    global smartphones_list
    smartphones_list.append(list(dataCleaning(maindict).values())) 

for j in range(1, 2):
    if (j <= 1):
        URL = 'https://www.euro.com.pl/telefony-komorkowe.bhtml'
    else:
        URL = 'https://www.euro.com.pl/telefony-komorkowe,strona-' + str(j) + '.bhtml'

    page = get(URL)
    bs = BeautifulSoup(page.content, "html.parser")

    for offer in bs.find_all('a', class_='call-button js-call'):
        try:
            getInside(offer['data-event'].split('|')[-1].split('/')[-1].split('.')[0].split(',')[0],
                      offer['data-event'].split('|')[3], offer['data-event'].split('|')[4])
        except Exception:
            print("EXCEPTION HAS OCCURED")
    print("STRONA: ", j,"/ 20")

transformed_data_container = []  
def Transform_to_staging (final_csv_list):
    for i in final_csv_list:
        single_phone_container = []
        
        # wydzielenie dwóch atrybuów z atrybutu operating_system
        operaring_system = i[1].split()
        operaring_system_type = operaring_system[0]
        operaring_system_version = " ".join(operaring_system[1:])
        if operaring_system_version == "":
            operaring_system_version = "NAN"

        # stworzenie dwóch atrybutów z RAM
        ram = i[2].replace(",", ".").upper().split()
        if ram[0] == "NAN":
            ram_gb = ram[0]
            ram_mb = ram[0]

        elif "GB" == ram[1]:
            ram_gb = ram[0]
            ram_mb = float(ram[0])*1000

        elif "MB" == ram[1]:
            ram_gb = float(ram[0])/1000
            ram_mb = ram[0]

        else:
            ram_gb = ram[0]
            ram_mb = ram[0]

        # usunięcie GB z pamięci wewnętrznej
        build_in_memory = i[3].upper().replace(" ", "").replace("GB", "")

        # podział przękątnej ekranu w zależności od liczby ekranów
        screen_diagonal = i[4].replace(",", ".")         
        number_of_screens = "indefinite" # w skrapowanych rtv_euro_agd danych jest tylko jeden wyświetlacz co nie daje jasnej odpowiedzi odnośnie ewentualnego drugiego
        main_screen = screen_diagonal
        scnd_screen = "indefinite"


        # utworzenie dodatkowych kolumn dla aparatu tylnego
        back_camera = i[6].upper().replace(" ", "").replace("MPIX", "").split("+")
        back_camera_main_lens = back_camera[0]
        back_camera_lens_amount = len(back_camera)


        # utworzenie dodatkowych kolumn dla aparatu przedniego
        front_camera = i[7].upper().replace(" ", "").replace("MPIX", "").split("+")
        front_camera_main_lens = front_camera[0]
        front_camera_lens_amount = len(front_camera)

        # usunięcie mAh z pojemności baterii
        battery = i[8].upper().replace(" ", "").replace(",", ".").replace("MAH", "")


        # podział rozdzielczości ekranu w zaleznosci od liczby ekranów 
        screen_res = i[10]
        main_screen_res = screen_res
        scnd_screen_res = "indefinite"


        single_phone_container = [i[0], operaring_system_type, operaring_system_version, ram_gb, ram_mb, build_in_memory, i[4].replace(",", "."), number_of_screens, main_screen,
        scnd_screen, i[5], i[6], back_camera_main_lens, back_camera_lens_amount, i[7], front_camera_main_lens, front_camera_lens_amount, battery, i[9],
        main_screen_res, scnd_screen_res, i[11], datetime.now().strftime("%Y_%m_%d"), "rtv_euro_agd"]

        transformed_data_container.append(single_phone_container)
    global smartphones_list
    smartphones_list = transformed_data_container
Transform_to_staging(smartphones_list)


with open(os.path.join(file_path,file_name), "wt", newline="",encoding='UTF-8') as fp:
    writer = csv.writer(fp)
    writer.writerows(smartphones_list)