# media_expert (NORBERT)
import os
from typing import Text
from bs4 import BeautifulSoup
from requests import get
from datetime import datetime
import csv

now_datetime = datetime.now()
now_datetime = now_datetime.strftime("%Y_%m_%d")

URL_without_idx = "https://www.mediaexpert.pl/smartfony-i-zegarki/smartfony?page="

smartpones_list = []
all_products_attributes = set({})

file_path = "WebScrap/"+str(datetime.now().year)+"/"+str(datetime.now().month)+"/"+str(datetime.now().day)

try:
    os.makedirs(file_path)
except Exception:
    pass

print(" -------------- BEGIN OF SCRIPT 1 ---------------------- ")

def addiding_attribute(container_name, attribute_dict, wanted_attributes_list):
    for i in wanted_attributes_list:
        try:
            container_name.append(attribute_dict[i])
        except:
            container_name.append("NAN")


wanted_attributes_list = ["Wersja systemu", "Pamięć RAM", "Pamięć wbudowana [GB]", "Przekątna ekranu [cal]",
                          "Aparat tylny", "Aparat przedni", "Pojemność akumulatora [mAh]", "Kolor obudowy",
                          "Rozdzielczość ekranu"]


def Scan_all_category(URL_idx):
    page = get(URL)
    bs = BeautifulSoup(page.content, "html.parser")
    amount_offers = 0

    for smartphone in bs.find_all('div', class_='offer-box'):
        smartphone_data = []

        smartphone_name = smartphone.find('a', class_='is-animate spark-link').get_text().strip().replace("Smartfon",
                                                                                                          "").replace(
            "Telefon", "").replace("″", "")

        try:

            smartphone_price = smartphone.find('div', class_='main-price is-big').get_text().replace(" zł", "").replace(
                " ", "").replace(" ", "").replace("″", "")
            smartphone_price = float(smartphone_price) / 100
        except Exception as exc:
            # print(exc)
            print("lack price , product unavailable ")
            smartphone_price = "NAN"
            continue

        phone_url = 'https://www.mediaexpert.pl' + smartphone.find('a', class_='is-animate spark-link').get('href')


        page_single_phone = get(phone_url)
        bs_single_phone = BeautifulSoup(page_single_phone.content, "html.parser")
        phone_attributes = {}

        for right_line in bs_single_phone.find_all('div', class_='specification'):

            attribute_name = "NAN"
            attribute_text = "NAN"

            for another_line in right_line.find_all('tr', class_='attribute'):

                for att_name in another_line.find_all('th', class_='name'):
                    attribute_name = att_name.get_text().strip().replace("\n", "").replace("  ", "")
                    all_products_attributes.add(attribute_name)

                for att_text in another_line.find_all('td', class_='values'):
                    attribute_text = att_text.get_text().strip().replace("\n", "").replace("  ", "")

                phone_attributes[attribute_name] = attribute_text

        addiding_attribute(smartphone_data, phone_attributes, wanted_attributes_list)

        smartphone_data.insert(0, smartphone_name)
        smartphone_data.insert(5, smartphone_price)
        smartphone_data.insert(11, phone_url)
        smartphone_data.append(now_datetime)
        smartphone_data.append("media_expert")

        smartpones_list.append(smartphone_data)

        amount_offers += 1

    if amount_offers == 0:  # kiedy jest wyświetlimy wszystkie telefony na każdej kolejnej "nieistniejącej" stronie wyświetla się tylko jedna która nie jest wliczana do amount offers
        global break_point
        break_point += 1


break_point = 0
for i in range(33, 1000):

    if break_point == 1:
        break

    URL = URL_without_idx + str(i)
    Scan_all_category(URL)
    print("web page", i + 1)



############################ tranformacja danych

transformed_data_container = []  
def Transform_to_staging (final_csv_list):
    for i in final_csv_list:
        single_phone_container = []
        
        # wydzielenie dwóch atrybuów z atrybutu operating_system
        operaring_system = i[1].split()
        operaring_system_type = operaring_system[0]
        operaring_system_version = " ".join(operaring_system[1:])
        if operaring_system_version == "":
            operaring_system_version = "NAN"

        # stworzenie dwóch atrybutów z RAM
        ram = i[2].upper().split()
        if ram[0] == "NAN":
            ram_gb = ram[0]
            ram_mb = ram[0]

        elif "GB" == ram[1]:
            ram_gb = ram[0]
            ram_mb = float(ram[0])*1000

        elif "MB" == ram[1]:
            ram_gb = float(ram[0])/1000
            ram_mb = ram[0]

        else:
            ram_gb = ram[0]
            ram_mb = ram[0]

        # podział przękątnej ekranu w zależności od liczby ekranów
        screen_diagonal = i[4].split(",")
        if len(screen_diagonal) == 1:
            number_of_screens = 1
            main_screen = screen_diagonal[0]
            scnd_screen = "NAN"
        else:
            number_of_screens = 2
            main_screen = screen_diagonal[0]
            scnd_screen = screen_diagonal[1]

        # utworzenie dodatkowych kolumn dla aparatu tylnego
        back_camera = i[6].upper().replace(" ", "").replace("MPX", "").split("+")
        back_camera_main_lens = back_camera[0]
        back_camera_lens_amount = len(back_camera)


        # utworzenie dodatkowych kolumn dla aparatu przedniego
        front_camera = i[7].upper().replace(" ", "").replace("MPX", "").split("+")
        front_camera_main_lens = front_camera[0]
        front_camera_lens_amount = len(front_camera)

        # podział rozdzielczości ekranu w zaleznosci od liczby ekranów 
        screen_res = i[10].split(",")
        if len(screen_res) == 1:
            main_screen_res = screen_res[0]
            scnd_screen_res = "NAN"
        else:
            main_screen_res = screen_res[0]
            scnd_screen_res = screen_res[1]

        single_phone_container = [i[0], operaring_system_type, operaring_system_version, ram_gb, ram_mb, i[3], i[4], number_of_screens, main_screen,
        scnd_screen, i[5], i[6], back_camera_main_lens, back_camera_lens_amount, i[7], front_camera_main_lens, front_camera_lens_amount, i[8], i[9],
        main_screen_res, scnd_screen_res, i[11], i[12], i[13]]

        transformed_data_container.append(single_phone_container)
    
Transform_to_staging(smartpones_list)
smartpones_list = transformed_data_container

print("all offers scraped")

file_name = now_datetime + "_media_expert.csv"

with open(os.path.join(file_path,file_name), "wt", newline="",encoding='UTF-8') as fp:
    writer = csv.writer(fp)
    writer.writerows(smartpones_list)

with open("all_products_attributes.csv", "wt", newline="",encoding='UTF-8') as fp:
    writer = csv.writer(fp)
    writer.writerows(all_products_attributes)

print("fies_created")
