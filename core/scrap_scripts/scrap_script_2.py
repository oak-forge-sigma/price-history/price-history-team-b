# Skapiec (Filip)

import os
from requests import get
from bs4 import BeautifulSoup
import pandas as pd
from datetime import datetime
import csv
print(" -------------- BEGIN OF SCRIPT 2 ---------------------- ")

file_path = "WebScrap/"+str(datetime.now().year)+"/"+str(datetime.now().month)+"/"+str(datetime.now().day)

now_datetime = datetime.now()
now_datetime = now_datetime.strftime("%Y_%m_%d")

s_name = []
s_price = []
s_att = []
s_link = []
url2 = 'https://www.skapiec.pl'
new_att = []


def parse_page(number):
    # print(f'Scrapuje strone numer : {number}')

    url = 'https://www.skapiec.pl/cat/26136/page/'
    page = get(f'{url}{number}')
    bs = BeautifulSoup(page.content, 'html.parser')

    for h2 in bs.findAll('h2', class_='title gtm_red_solink'):
        s_name.append(h2.get_text())
    for price in bs.findAll('strong', class_='price gtm_sor_price'):
        s_price.append((price.get_text().replace("od ", "").replace(" zł", "")))
    for v in bs.findAll('table', class_='tech'):
        s_att.append(((v.get_text().strip().replace("Producent: ", "").replace("Ekran:", "").replace('"', " ").replace(
            "Aparat cyfrowy tylny:", "").replace("Aparat cyfrowy przedni:", "").replace("Pamięć RAM:", "").replace(
            "Pojemność akumulatora:", "").replace(" mAh", "mAh").replace("System operacyjny:", "").replace(
            "Wbudowana pamięć:", "")).replace(" Mpix", "Mpix").replace(" GB", "GB")).split())

    for link in bs.findAll('a', class_='box'):
        s_link.append(url2 + link.get('href'))


for number in range(1, 44):
    parse_page(number)

s_att2 = [x for x in s_att if len(x) < 11]
df_nazwa = pd.DataFrame(s_name, columns=['Nazwa'])
df_cena = pd.DataFrame(s_price, columns=['Cena'])
df_att = pd.DataFrame(s_att2, columns=['Producent', 'Ekran', 'Piksele', 'Aparat tyl', 'Aparat przod', 'RAM', 'Bateria',
                                       'System', 'System2',
                                       'Pamięć wbudowana'])
df_link = pd.DataFrame(s_link, columns=['Link'])
df = pd.concat([df_nazwa, df_cena, df_att, df_link], axis=1)
df["Kolor"] = 'NAN'
df["Sklep"] = 'skapiec'
# df["System"] = df["System"] + ' ' + df["System2"]
from datetime import datetime

#df['Data'] = datetime.today().strftime('%d-%m-%Y')
df['Data'] = datetime.now().strftime("%Y_%m_%d")

final_df = df[
    ['Nazwa', 'System', 'RAM', 'Pamięć wbudowana', 'Ekran', 'Cena', 'Aparat tyl', 'Aparat przod', 'Bateria', 'Kolor',
     'Piksele', 'Link', 'Data', 'Sklep']]

file_name = now_datetime + "_skapiec.csv"

#final_df.to_csv(os.path.join(file_path,file_name), index=False, header=False)


########################################################################################


smartphones_list = []
transformed_data_container = []  
def Transform_to_staging (final_csv_list):
    for i in final_csv_list:
        single_phone_container = []
        
        # wydzielenie dwóch atrybuów z atrybutu operating_system
        operaring_system = i[1]
        operaring_system_type = operaring_system
        operaring_system_version ="NAN"

        # stworzenie dwóch atrybutów z RAM

        try:
            ram = str(i[2]).replace(",", ".").upper()

            if  "GB" in ram:
                ram = ram.replace("GB", (""))
                ram_gb = ram
                ram_mb = float(ram)*1000

            elif "MB" in ram:
                ram.replace("MB", (""))
                ram_gb = float(ram[0])/1000
                ram_mb = ram[0]

            else:
                ram_gb = ram
                ram_mb = ram
            
        except:
            ram_gb = ram
            ram_mb = ram

        # usunięcie GB z pamięci wewnętrznej
        try:
            build_in_memory = i[3].upper().replace(" ", "").replace("GB", "")
        except:
            build_in_memory = i[3]

        # podział przękątnej ekranu w zależności od liczby ekranów
        try:
            screen_diagonal = i[4].replace(",", ".")
        except:
            screen_diagonal = i[4]

        number_of_screens = "indefinite" # w skrapowanych rtv_euro_agd danych jest tylko jeden wyświetlacz co nie daje jasnej odpowiedzi odnośnie ewentualnego drugiego
        main_screen = screen_diagonal
        scnd_screen = "indefinite"

        # zamiana , na . w cenie
        price = str(i[5]).replace(",", ".").replace(" ", "")

        # utworzenie dodatkowych kolumn dla aparatu tylnego
        try:
            back_camera = i[6].upper().replace(" ", "").replace("MPIX", "").split("+")
            back_camera_main_lens = back_camera[0]
            back_camera_lens_amount = len(back_camera)
        except:
            back_camera = i[6]
            back_camera_main_lens = "NAN"
            back_camera_lens_amount = "NAN"

        # utworzenie dodatkowych kolumn dla aparatu przedniego
        try:
            front_camera = i[7].upper().replace(" ", "").replace("MPIX", "").split("+")
            front_camera_main_lens = front_camera[0]
            front_camera_lens_amount = len(front_camera)
        except:
            front_camera = i[7]
            front_camera_main_lens = "NAN"
            front_camera_lens_amount = "NAN"
        # usunięcie mAh z pojemności baterii
        try:
            battery = i[8].upper().replace(" ", "").replace(",", ".").replace("MAH", "")
        except:
            battery = i[8]


        # podział rozdzielczości ekranu w zaleznosci od liczby ekranów 
        screen_res = i[10]
        main_screen_res = screen_res
        scnd_screen_res = "indefinite"


        single_phone_container = [i[0], operaring_system_type, operaring_system_version, ram_gb, ram_mb, build_in_memory, screen_diagonal, number_of_screens, main_screen,
        scnd_screen, price, i[6], back_camera_main_lens, back_camera_lens_amount, i[7], front_camera_main_lens, front_camera_lens_amount, battery, i[9],
        main_screen_res, scnd_screen_res, i[11], datetime.now().strftime("%Y_%m_%d"), "skąpiec"]

        transformed_data_container.append(single_phone_container)
    global smartphones_list
    smartphones_list = transformed_data_container

Transform_to_staging(final_df.values.tolist())


with open(os.path.join(file_path,file_name), "wt", newline="",encoding='UTF-8') as fp:
    writer = csv.writer(fp)
    writer.writerows(smartphones_list)