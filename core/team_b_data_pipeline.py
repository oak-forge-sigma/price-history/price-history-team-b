from datetime import datetime, timedelta
from airflow import DAG
from airflow.hooks.postgres_hook import PostgresHook
from airflow.operators.python_operator import PythonOperator
from airflow.operators.postgres_operator import PostgresOperator
import os
import csv


from typing import Text
from bs4 import BeautifulSoup
from requests import get

smartpones_list =[]
now_datetime = datetime.now()
now_datetime = now_datetime.strftime("%Y_%m_%d")

URL_without_idx = "https://www.mediaexpert.pl/smartfony-i-zegarki/smartfony?page="



default_args={
    'owner' : "oak_price_history_team_b",
    'start_date' : datetime(2022,1,1),
    'retries' : 0
}
dl_path = "/usr/local/spark/resources/team_b/WebScrap"

def DOWNLOAD_DATA():
    get_brands_names ="select brand from price_history.reg_brands" 
    get_brands_exceptions = "select wrong_brand, correct_brand from price_history.reg_brand_exceptions" 
    get_colors = "select color from price_history.reg_colors"

    hook = PostgresHook(postgres_conn_id='postgres_team_b')
    conn = hook.get_conn()
    cur = conn.cursor()

    cur.execute(get_brands_names)
    brands_names = cur.fetchall()
    brands_names = [item[0] for item in brands_names]


    cur.execute(get_brands_exceptions)
    brands_exceptions = cur.fetchall()
    brands_exceptions = dict((wrong, right) for wrong, right in brands_exceptions)

    cur.execute(get_colors)
    colors = cur.fetchall()
    colors = [item[0] for item in colors]
 
    addition_items = ["smartwatch", "opaska", "band"]

    file_path = "/usr/local/spark/resources/team_b/WebScrap/"+str(datetime.now().year)+"/"+str(datetime.now().month)+"/"+str(datetime.now().day)

    try:
        os.makedirs(file_path)
    except Exception:
        pass
    missing_brands_in_DWH = []
    print(" -------------- BEGIN OF SCRIPT 1 ---------------------- ")

    def Scan_all_category(URL):
        page = get(URL)
        bs = BeautifulSoup(page.content, "html.parser")
        amount_offers = 0
        for smartphone in bs.find_all('div', class_='offer-box'):
            smartphone_data = []
            brand = "NAN"
            model = "NAN"
            color = "NAN"
            # get smarphone fulltext
            name_fulltext = smartphone.find('a', class_='is-animate spark-link').get_text().strip().replace("Smartfon ",
                                    "").replace("Telefon ", "").replace("″", "").replace("Smartfon","").replace("Telefon", "")

            # get smarphone price
            try:
                smartphone_price = smartphone.find('div', class_='main-price is-big').get_text().replace(" zł", "").replace(
                    " ", "").replace(" ", "").replace("″", "")
                smartphone_price = float(smartphone_price) / 100
            except Exception as exc:
                try:
                    smartphone_price = smartphone.find('div', class_='main-price for-action-price is-big').get_text().replace(" zł", "").replace(
                        " ", "").replace(" ", "").replace("″", "")
                    smartphone_price = float(smartphone_price) / 100
                except Exception as exc:
                    smartphone_price = "NAN"
                    print("lack price , product unavailable ")
                    continue

            # entry for smartphone page
            phone_url = 'https://www.mediaexpert.pl' + smartphone.find('a', class_='is-animate spark-link').get('href')
            page_single_phone = get(phone_url)
            bs_single_phone = BeautifulSoup(page_single_phone.content, "html.parser")
            phone_attributes = {"System operacyjny":"NAN","Wersja systemu":"NAN","Pamięć RAM":"NAN","Pamięć wbudowana [GB]":"NAN",
                                "Wyświetlacz":"NAN", "Aparat":"NAN", "Pojemność akumulatora [mAh]":"NAN", "Kolor obudowy":"NAN"}
            
            #get color
            for page_data_color_1 in bs_single_phone.find_all('div', class_='column-right'):
                for page_data_color_2 in page_data_color_1.find_all('div', class_='variants'):
                    for page_data_color_3 in page_data_color_2.find_all('div', class_='header', limit =1):
                        for page_data_color_4 in page_data_color_3.find_all('span', class_='current is-regular'):
                            page_data_color_5 = page_data_color_4.get_text()
                            if page_data_color_5 not in ["Tak", "Nie"] and page_data_color_5[0].isnumeric() != True :
                                color = page_data_color_5
            if color == "NAN":
                all_colors = []
                for col in colors:
                    if col in name_fulltext.upper():
                        all_colors.append(col)
                try:
                    color = max(all_colors, key=len)
                except:
                    color = "NAN"

            #get phone_attributes
            for x1 in bs_single_phone.find_all('table', class_='list attributes'):
                for x2 in x1.find_all('th'):
                    atrybut = x2.get_text().replace("\n", "").replace("  ", "").replace(":","")
                    if atrybut in phone_attributes.keys():
                        next_text = x2.findNext('td')
                        attribute_text = next_text.get_text().strip().replace("\n", "").replace("  ", "")
                        phone_attributes[atrybut] = attribute_text
                        
            # czyszczenie i wydrębnianie danych
            # wydzielenie wersji systemu
            operaring_system = phone_attributes["Wersja systemu"].split()
            operaring_system_version = " ".join(operaring_system[1:])
            if operaring_system_version == "":
                operaring_system_version = "NAN"
            operaring_system = operaring_system[0]
            # stworzenie dwóch atrybutów z RAM
            ram = phone_attributes["Pamięć RAM"].upper().split()
            if ram[0] == "NAN":
                ram_gb = ram[0]
                ram_mb = ram[0]

            elif "GB" == ram[1]:
                ram_gb = ram[0]
                ram_mb = float(ram[0])*1000

            elif "MB" == ram[1]:
                ram_gb = float(ram[0])/1000
                ram_mb = ram[0]

            else:
                ram_gb = ram[0]
                ram_mb = ram[0]

            screen = phone_attributes["Wyświetlacz"].split(", ")
            screen_size = screen[0].replace("\"", "")
            try:
                screen_resolution = screen[1].replace("px", "")
            except:
                screen_resolution = "NAN"

            camera = phone_attributes["Aparat"]
            camera = camera.split(", Przedni")
            camera_back = camera[0].replace("Tylny ", "")

            try:
                camera_front = camera[1]
                camera_front = camera_front[1:]
            except:
                camera_front = "NAN" 

            camera_back_main = camera_back.replace("Mpx","").replace("X","").replace("x","").replace("+","").split()

            try:
                camera_back_main = [float(i) for i in camera_back_main]
                camera_back_main = max(camera_back_main)
            except:
                camera_back_main ="NAN"

            camera_front_main = camera_front.replace("Mpx","").replace("X","").replace("x","").replace("+","").split()

            try:
                camera_front_main = [float(i) for i in camera_front_main]
                camera_front_main = max(camera_front_main)
            except:
                camera_front_main ="NAN"

            
            name_fulltext_edit = name_fulltext.upper().replace("(","").replace(")","")
            if name_fulltext_edit.startswith(" "):
                name_fulltext_edit = name_fulltext_edit[1:]

            
            if name_fulltext_edit.startswith("GSM "):
                name_fulltext_edit = name_fulltext_edit[5:]
            
        # sprawdzanie czy smarfon znajduje się w liści telefonów csv i liście wyjątków
            if not any(elem in name_fulltext_edit for elem in brands_names):
                missing_brands_in_DWH.append(name_fulltext_edit)
                continue
                            
            else:
                # rozbicie full texru na elementy listy 
                name_fulltext_edit = name_fulltext_edit.split()
                part_of_brand_and_model = []
                
                # sprawdzenie kolejncyh słów full textu (już listy)
                for element in name_fulltext_edit:

                    # sprzawdzenie który element listy zawiera kluczowy ciąg znaków po którym następuje rozbicie nazwy    
                    if "GB" in element or "TB" in element or "/" in element or (element in colors):
                        break
                    else:
                        # dodanie dotyczasowych elementów listy przed breakiem 
                        part_of_brand_and_model.append(element)

                # ustanowienie nowego elementu głównej listy - full textu
                name_fulltext_edit = ' '.join(part_of_brand_and_model)
            
            for brand_name in brands_names:
                if name_fulltext_edit.startswith(brand_name):
                    brand = brand_name
                    model = name_fulltext_edit.split(brand)[1][1:]
                    if brand in brands_exceptions.keys():
                        model = brand + " " + model
                        brand = brands_exceptions[brand]
                    for i in addition_items:
                        if i in phone_url:
                            if i.upper() not in model:
                                model = f"{model} + {i.upper()}"
                                
            smartphone_data.insert(0, brand)
            smartphone_data.insert(1, model)
            smartphone_data.insert(2, operaring_system.upper())
            smartphone_data.insert(3, operaring_system_version.upper() )
            smartphone_data.insert(4, ram_gb)   
            smartphone_data.insert(5, ram_mb)  
            smartphone_data.insert(6, phone_attributes["Pamięć wbudowana [GB]"]) 
            smartphone_data.insert(7,"NAN") # screen size diagonal full scraped txt
            smartphone_data.insert(8,"NAN") # numbers of screens
            smartphone_data.insert(9, screen_size)                                                      
            smartphone_data.insert(10,"NAN")    #scnd screen szie   
            smartphone_data.insert(11, smartphone_price)                                                     
            smartphone_data.insert(12, camera_back.upper())                                                       
            smartphone_data.insert(13, camera_back_main)                                                             
            smartphone_data.insert(14, "NAN")  #camera_back_numbers_of_screens                                           
            smartphone_data.insert(15, camera_front.upper())                                                            
            smartphone_data.insert(16, camera_front_main)                                                               
            smartphone_data.insert(17, "NAN")  #camera front numbers od screens
            smartphone_data.insert(18, phone_attributes["Pojemność akumulatora [mAh]"])
            smartphone_data.insert(19, color.upper())
            smartphone_data.insert(20, screen_resolution)                                                               
            smartphone_data.insert(21, "NAN")  #scnd screen resolution
            smartphone_data.insert(22, phone_url)
            smartphone_data.append(now_datetime)
            smartphone_data.append("media_expert")

            smartpones_list.append(smartphone_data)
            #smartphone_data.insert(0, name_fulltext.upper())
            amount_offers += 1

    for i in range(0, 36):
        URL = URL_without_idx + str(i)
        Scan_all_category(URL)
        print("web page", i + 1)

    print("all offers scraped")

    file_name = now_datetime + "_media_expert.csv"



    smarpone_list = set(tuple(i) for i in smartpones_list)
    smarpone_list = list(list(i) for i in smartpones_list)

    with open(os.path.join(file_path,file_name), "wt", newline="",encoding='UTF-8') as fp:
        writer = csv.writer(fp)
        writer.writerows(smarpone_list)

    print("fies_created")
    print("missing brands:",missing_brands_in_DWH)

    ####################################

    missing_brands_in_DWH = []

    file_path = "/usr/local/spark/resources/team_b/WebScrap/"+str(datetime.now().year)+"/"+str(datetime.now().month)+"/"+str(datetime.now().day)

    ct = datetime.now()
    file_name = str(ct).split()[0].replace("-", "_") + "_rtv_euro_agd.csv"

    try:
        os.makedirs(file_path)
    except Exception:
        pass

    def dataCleaning(dicten):
        dicten["Przekątna wyświetlacza"] = dicten["Przekątna wyświetlacza"].split()[0]
        dicten["Rozdzielczość ekranu"] = dicten["Rozdzielczość ekranu"].split('pikseli')[0]
        dicten["Pojemność baterii"] = dicten["Pojemność baterii"].replace(' mAh', '')
        dicten["Pamięć RAM"] = dicten["Pamięć RAM"].replace('poniżej', '')
        return dicten

    smartphones_list = []
    def getInside(url, name, price):
        URL = 'https://www.euro.com.pl/telefony-komorkowe/' + url + '.bhtml'

        keyList = {"Pojemność baterii", "System operacyjny", "Przekątna wyświetlacza", "Rozdzielczość ekranu", "Pamięć RAM",
                "Pamięć wbudowana", "Pojemność baterii", "Aparat tylny", "Wyświetlacz",
                "Kolor obudowy", "Aparat przedni"}
        maindict = {"nazwa": name, "System operacyjny": "NAN", "Pamięć RAM": "NAN", "Pamięć wbudowana": "NAN",
                    "Przekątna wyświetlacza": "NAN", "Cena": price, "Aparat tylny": "NAN", "Aparat przedni": "NAN",
                    "Pojemność baterii": "NAN", "Kolor obudowy": "NAN", "Rozdzielczość ekranu": "NAN", "link": URL}

        page = get(URL)
        bs = BeautifulSoup(page.content, "html.parser")
        
        for offer in bs.find_all('td'):
            if offer.text.strip() in maindict.keys():
                b_tag = offer
                next_td_tag = b_tag.findNext('td')
                maindict[offer.text.strip()] = next_td_tag.text.strip()
    
        #writer.writerow(dataCleaning(maindict).values())
        smartphones_list.append(list(dataCleaning(maindict).values())) 

    for j in range(0,20):
        if (j <= 1):
            URL = 'https://www.euro.com.pl/telefony-komorkowe.bhtml'
        else:
            URL = 'https://www.euro.com.pl/telefony-komorkowe,strona-' + str(j) + '.bhtml'

        page = get(URL)
        bs = BeautifulSoup(page.content, "html.parser")
        """
        for offer in bs.find_all('a', class_='call-button js-call'):
            try:
                getInside(offer['data-event'].split('|')[-1].split('/')[-1].split('.')[0].split(',')[0],
                        offer['data-event'].split('|')[3], offer['data-event'].split('|')[4])
            except Exception:
                print("EXCEPTION HAS OCCURED")
        print("STRONA: ", j,"/ 20")"""
        for i in bs.find_all('body', id='leaf'):
            for y in bs.find_all('div', class_='page'):
                for x in y('div', id='product-list'):
                    for x1 in x('div', id="products", class_='list'):
                        for x2 in x1('div', class_='product-for-list'):
                            for x3 in x2('div', class_='product-row'):
                                for x4 in x3('div', class_='product-main'):
                                    for x5 in x4('div', class_='product-community'):
                                        for x6 in x5('ul', class_ ='community-list'):
                                            for offer in x6('a', class_='call-button js-call'):
                                                try:
                                                    getInside(offer['data-event'].split('|')[-1].split('/')[-1].split('.')[0].split(',')[0],
                                                            offer['data-event'].split('|')[3], offer['data-event'].split('|')[4])
                                                except Exception:
                                                    print("EXCEPTION HAS OCCURED")
        print("STRONA: ", j,"/ 20")
    
    smartpones_list_1 = []
    for i in smartphones_list:
        smartphone_data = []
        brand = "NAN"
        model = "NAN"

        name_fulltext = i[0]
        name_fulltext_edit = name_fulltext.upper().replace("(","").replace(")","")
        if name_fulltext_edit.startswith(" "):
            name_fulltext_edit = name_fulltext_edit[1:]

        
        if name_fulltext_edit.startswith("GSM "):
            name_fulltext_edit = name_fulltext_edit[5:]
        
    # sprawdzanie czy smarfon znajduje się w liści telefonów csv i liście wyjątków
        if not any(elem in name_fulltext_edit for elem in brands_names):
            missing_brands_in_DWH.append(name_fulltext_edit)
            continue
                            
        else:
            # rozbicie full texru na elementy listy 
            name_fulltext_edit = name_fulltext_edit.split()
            part_of_brand_and_model = []
            
            # sprawdzenie kolejncyh słów full textu (już listy)
            for element in name_fulltext_edit:

                # sprzawdzenie który element listy zawiera kluczowy ciąg znaków po którym następuje rozbicie nazwy    
                if "GB" in element or "TB" in element or "/" in element or (element in colors):
                    break
                else:
                    # dodanie dotyczasowych elementów listy przed breakiem 
                    part_of_brand_and_model.append(element)

            # ustanowienie nowego elementu głównej listy - full textu
            name_fulltext_edit = ' '.join(part_of_brand_and_model)
        
        for brand_name in brands_names:
            if name_fulltext_edit.startswith(brand_name):
                brand = brand_name
                model = name_fulltext_edit.split(brand)[1][1:]
                if brand in brands_exceptions.keys():
                    model = brand + " " + model
                    brand = brands_exceptions[brand]
                    for i in addition_items:
                        if i in phone_url:
                            if i.upper() not in model:
                                model = f"{model} + {i.upper()}" 
                

        operaring_system = i[1].upper().split()
        operaring_system_version = " ".join(operaring_system[1:])
        if operaring_system_version == "":
                operaring_system_version = "NAN"
        operaring_system = operaring_system[0]

        # stworzenie dwóch atrybutów z RAM
        ram = i[2].upper().replace(",",".").split()
        if ram[0] == "NAN":
            ram_gb = ram[0]
            ram_mb = ram[0]

        elif "GB" == ram[1]:
            ram_gb = ram[0]
            ram_mb = float(ram[0])*1000

        elif "MB" == ram[1]:
            ram_gb = float(ram[0])/1000
            ram_mb = ram[0]

        else:
            ram_gb = ram[0]
            ram_mb = ram[0]

            
        build_in_memory = i[3].upper().replace(" ", "").replace("GB", "").replace(",",".")
        screen_size = i[4]
        smartphone_price = i[5]

        camera_back = i[6]
        camera_front = i[7]

        camera_back_main = camera_back.replace("Mpix","").replace("X","").replace("x","").replace("+","").replace(",",".").split()
    
        try:
            camera_back_main = [float(i) for i in camera_back_main]
            camera_back_main = max(camera_back_main)
        except:
            camera_back_main ="NAN"

        camera_front_main = camera_front.replace("Mpix","").replace("X","").replace("x","").replace("+","").split()

        try:
            camera_front_main = [float(i) for i in camera_front_main]
            camera_front_main = max(camera_front_main)
        except:
            camera_front_main ="NAN"
        
        battery = i[8]
        color = i[9].upper()
        screen_resolution = i[10]
        phone_url = i[11]


        smartphone_data.insert(0, brand)
        smartphone_data.insert(1, model)
        smartphone_data.insert(2,operaring_system)
        smartphone_data.insert(3, operaring_system_version.upper() )
        smartphone_data.insert(4, ram_gb)   
        smartphone_data.insert(5, ram_mb)  
        smartphone_data.insert(6, build_in_memory) 
        smartphone_data.insert(7,"NAN") # screen size diagonal full scraped txt
        smartphone_data.insert(8,"NAN") # numbers of screens
        smartphone_data.insert(9, screen_size)
        smartphone_data.insert(10,"NAN")    #scnd screen szie
        smartphone_data.insert(11, smartphone_price)
        smartphone_data.insert(12, camera_back.upper())
        smartphone_data.insert(13, camera_back_main)
        smartphone_data.insert(14, "NAN")  #camera_back_numbers_of_screens
        smartphone_data.insert(15, camera_front.upper()) 
        smartphone_data.insert(16, camera_front_main)
        smartphone_data.insert(17, "NAN")  #camera front numbers od screens
        smartphone_data.insert(18, battery)
        smartphone_data.insert(19, color.upper())
        smartphone_data.insert(20, screen_resolution)
        smartphone_data.insert(21, "NAN")  #scnd screen resolution
        smartphone_data.insert(22, phone_url)
        smartphone_data.append(now_datetime)
        smartphone_data.append("rtv_euro_agd")

        smartpones_list_1.append(smartphone_data)    
        smartpones_list_1 = tuple(smartpones_list_1)
        smartpones_list_1 = list(smartpones_list_1)

    with open(os.path.join(file_path,file_name), "wt", newline="",encoding='UTF-8') as fp:
        writer = csv.writer(fp)
        writer.writerows(smartpones_list_1)


    print("fies_created")
    print("missing models:",missing_brands_in_DWH)

    
def LOAD_CSVS_TO_STAGING():
    all_csvs = []
    for root, dirs, files in os.walk(dl_path):
        for file in files:
            if file[-3:] == "csv":
                all_csvs.append(root + "/" + file)

    SQL_get_loaded_files = "select file_name from price_history.reg_loaded_files"
 

    SQL_load_csv_to_staging = """COPY price_history.smartphones_details_daily_stg(
                            item_brand,
                            item_model,
                            operating_system_name,
                            operating_system_version,
                            ram_memory_gb,
                            ram_memory_mb,
                            built_in_memory_gb,
                            screen_size_diagonal_full_scraped_txt,
                            number_of_screens,
                            main_screen_size_inch,
                            scnd_screen_size_inch,
                            price,
                            camera_back_full_scraped_txt,
                            camera_back_mpx,
                            camera_back_number_of_screens,
                            camera_front_full_scraped_txt,
                            camera_front_mpx,
                            camera_front_number_of_screens,
                            battery_capacity,
                            color,
                            main_screen_resolution_mpx,
                            scd_screen_resolution_mpx,
                            source_smarthpone_url,
                            calendar_date,
                            dealer_name
                            ) FROM stdin with csv"""

    hook = PostgresHook(postgres_conn_id='postgres_team_b')
    conn = hook.get_conn()
    cur = conn.cursor()

    cur.execute(SQL_get_loaded_files)
    loaded_files = cur.fetchall()
    loaded_files = [item[0] for item in loaded_files]    

    files_to_load = list(set(all_csvs)-set(loaded_files))
    


    try:
        for file in files_to_load:
            with open(file, 'r', encoding='utf-8') as f:
                cur.copy_expert(SQL_load_csv_to_staging, f)
                conn.commit()
    except:
        print(f' \n something wrong with file \n {f}')    

    files_to_load = ["('"+i+"')" for i in files_to_load]
    files_to_load = ','.join(files_to_load)
    SQL_update_loaded_files = f'insert into price_history.reg_loaded_files(file_name) values {files_to_load}'
    cur.execute(SQL_update_loaded_files)
    conn.commit()

with  DAG(dag_id='team_b_data_pipeline',
        default_args=default_args,
        schedule_interval='1 0 * * *',
        catchup=False) as dag:

    download_data = PythonOperator(task_id='download_data', 
                          python_callable = DOWNLOAD_DATA)

    load_csvs_to_staging= PythonOperator(task_id='load_csvs_to_staging', 
                          python_callable = LOAD_CSVS_TO_STAGING)

    load_stg_to_final = PostgresOperator(task_id="load_stg_to_final",
                        postgres_conn_id="postgres_team_b",
                        sql="price_history_Populate_Facts_Dim.sql")

download_data >> load_csvs_to_staging >> load_stg_to_final
