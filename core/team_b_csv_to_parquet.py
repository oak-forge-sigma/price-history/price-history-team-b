import os
from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('team_b_csv_to_parquet').getOrCreate()

def transform_to_parquet():

  folder_path = '/usr/local/spark/resources/team_b/WebScrap'  
  files_names = [] 
  files_parquet = [] 

  #go through files in folder_path to find csv and parquet files
  for subdir, dirs, files in os.walk(folder_path):
    for file in files:
      filepath = subdir + os.sep + file
      if filepath.endswith(".csv"):
        files_names.append(filepath)
      if filepath.endswith(".parquet"):
        files_parquet.append(filepath)

#declare a path for folders that require transformation
  files_parquet_to_skip = []
  for f in files_parquet:
    files_parquet_to_skip.append(os.path.dirname(f))
  files_parquet_to_really_skip = []
  for f in files_parquet_to_skip:
    files_parquet_to_really_skip.append(os.path.dirname(f))

  files_names_with_csv=[] 
  for f in files_names:
    files_names_with_csv.append(os.path.dirname(f))

# Based on files_parquet_to_really_skip and files_names_with_csv
# Create output - substraction list 

  substr = [x for x in files_names_with_csv if not x in files_parquet_to_really_skip or files_parquet_to_really_skip.remove(x)]

#my_list - list without duplicated path 

  my_list = list(set(substr))
  my_list

#declare path for converting only files without previous convert csv to parquet 

  path_for_convert = []
  for el in my_list:
    for subdir, dirs, files in os.walk(el):
      for file in files:
        filepath2 = subdir + os.sep + file
        if file.endswith(".csv"):
          path_for_convert.append(filepath2)

#Append parquet files

  for i in path_for_convert:
    df = spark.read.format("csv").load(i)  
    df.write.mode("append").parquet(i[:-4]+'.parquet')

transform_to_parquet()
