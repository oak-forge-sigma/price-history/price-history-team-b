import csv
import re
import os


brand_exception = {"HAMMER" : "MYPHONE", "Q-SMART" : "MYPHONE"}

colors = ["beżowy", "złoty", "niebieski", "szary", "czarny", "żółty", "biały", "grafitowy", "srebrny", "czarno-srebrny", "błękitny", "fioletowo-niebieski",
 "pomarańczowy", "zielony", "czerwony", "czarno-niebieski", "purpurowy", "czarno-czerwony", "srebrno-Pomarańczowy", "czarne", "purpurowy",
 "brązowy", "czarno-pomarańczowy", "czarno-zielony", "fioletowy", "grafitowy", "lawendowy", "róźowy", "czarno-szary", "turkusowy", "oliwkowy",
 "perłowy", "stalowy" , "czerń", "różowy", "biało-złoty", "limonkowy"]

colors_with_bracekts = list("(" + x + ")" for x in colors )

script_dir = os.path.dirname(__file__)
previous_transformed_files_dir = "previous_transformed_files.csv"
abs_file_path_prev_trans_files = os.path.join(script_dir, previous_transformed_files_dir)



# ładowanie pliku z wczesniej transformowanymi danymi
previous_loaded_files =[]
try:
	with open(abs_file_path_prev_trans_files, newline='') as f:
		reader = csv.reader(f)
		previous_loaded_files = list(reader)
		previous_loaded_files = [item for sublist in previous_loaded_files for item in sublist]
except:
    print("file doesn't find")

# stworzenie listy marek z pliku

smrt_brands_list_dir = "phones_brands_list.csv"
abs_file_path_smrt_brands = os.path.join(script_dir, smrt_brands_list_dir)

smartphones_brands_list =[]
with open(abs_file_path_smrt_brands, newline='') as f:
    reader = csv.reader(f)
    smartphones_brands_list = list(reader)
    # wydrębienie i zamiana liter na duże wszystkich telefonów
    smartphones_brands_list = [item.upper() for sublist in smartphones_brands_list for item in sublist]
    

# tworzenie ścieżek do wszystkich plików
all_existing_files = []
for root, dirs, files in os.walk("WebScrap"):
	for file in files:
		all_existing_files.append(root + "\\" + file)

# wczytanie csv do listy single_file_records

for file in all_existing_files:
    if file not in previous_loaded_files:
        with open(file, 'r+', newline="", encoding='utf-8') as f:
                single_file_records = []       
                csvreader = csv.reader(f)
                print( "file name - ", file)
                try:
                    for row in csvreader:
                        single_file_records.append(row)
                except: 
                    print("loading error file -", file, "row -", row)
                f.close()

                for record in single_file_records:
                    if record[0].startswith(" "):
                        record[0] = record[0][1:]
                    record[0] = record[0].upper()
                    
                    # sprzawdzenie czy występuje wyjątek 
                    for exc in list(brand_exception.keys()):
                        if record[0].startswith(exc):
                            
                            record[0] = brand_exception[exc] + " " + exc
                            
                # sprawdzanie czy smarfon znajduje się w liści telefonów csv i liście wyjątków
                    if not any(elem in record[0] for elem in smartphones_brands_list):
                        if not any( record[0].startswith(i) for i in brand_exception.keys()):
                            print("lack brand in csv brands list", record[0])

                    else:
                        
                        # rozbicie full texru na elementy listy 
                        record[0] = record[0].split()
                        part_of_brand_and_model = []

                        # sprawdzenie kolejncyh słów full textu (już listy)
                        for i in record[0]:
                          
                            element = i.lower()

                            # sprzawdzenie który element listy zawiera kluczowy ciąg znaków po którym następuje rozbicie nazwy    
                            if "gb" in element or "tb" in element or (element in colors) or (element in colors_with_bracekts) :
                                break
                            else:
                                # dodanie dotyczasowych elementów listy przed breakiem 
                                part_of_brand_and_model.append(i)

                        # ustanowienie nowego elementu głównej listy - full textu
                        record[0] = ' '.join(part_of_brand_and_model)
                    
                    for brand_name in smartphones_brands_list:
                        if record[0].startswith(brand_name):

                            brand = brand_name
                            model = record[0].split(brand)[1]
                            record[0] = brand
                            
                            record.insert(1, model)

                    # nie wiem czemu ale nie mogłem zapisać pliku w trybie r+....
                    with open(file, 'r+', newline="", encoding='utf-8') as f:
                        f.truncate(0)
                        writer = csv.writer(f)
                        writer.writerows(single_file_records)


with open('previous_transformed_files.csv', "a+", newline='') as f:
	writer = csv.writer(f)
	for file in all_existing_files:
		try:
			if file not in previous_loaded_files:
				writer.writerow([file])
		except:
			writer.writerow([file])