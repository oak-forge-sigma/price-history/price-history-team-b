from datetime import timedelta
from airflow import DAG
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from datetime import datetime

spark_master = "spark://spark:7077"

default_args={
    'owner' : "oak_price_history_team_b",
    'start_date' : datetime(2022,1,1),
    'retries' : 1,
    'retry_delay' : timedelta(minutes=30)
}


dag = DAG(
    dag_id= 'team_b_transform_to_parquet',
    default_args=default_args,
    description = 'Dag converting web-scraped csv files into parquet each day at midnight',
    schedule_interval='0 0 * * *',
    catchup=False)

team_b_task_transform_csv_to_parquet = SparkSubmitOperator(
    task_id = 'team_b_transform_csv_to_parquet',
    conn_id = 'spark_default',
    application ='/usr/local/spark/app/team_b_csv_to_parquet.py',
    name = 'team_b_csv_to_parquet',
    conf = {'spark.master': spark_master},
    verbose=1,
    dag=dag)

team_b_task_transform_csv_to_parquet

