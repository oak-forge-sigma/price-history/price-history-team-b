DROP TABLE IF EXISTS price_history.smartphones_weekly_aggregates;
CREATE TABLE price_history.smartphones_weekly_aggregates(
	record_id serial primary key,
	"Year" numeric,
	"Week of year" numeric,
	"Time interval" varchar(50),
	"Brand" varchar(50),
	"Model" varchar(50),
	"Color" varchar(50),
	"Ram" varchar(50),
	"Memory" varchar(50),
	"ID others att" varchar(50),
	"Average last week price" numeric ,
	"Minimum  last week price" numeric,
	"Maximum last week price" numeric,
	"The Lowest price Store " varchar(50)
	);
