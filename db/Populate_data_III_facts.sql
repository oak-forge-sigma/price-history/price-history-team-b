with 
	staging as 
		(
		select distinct	
			item_brand as s10,
			item_model as s11,
			color as s12,
			ram_memory_gb as s13,
			built_in_memory_gb as s14,
			dealer_name,
			calendar_date,
			price
		from price_history.smartphones_details_daily_stg
		),	
		
	items as 
		(
		select distinct
			id,
			item_brand as i10,
			item_model as i11,
			color as i12,
			ram_memory_gb as i13,
			built_in_memory_gb as i14
		from price_history.dim_items
		)
		
insert into
	price_history.Items_FactTable
		(
		id_items,
		calendar_date,
		shop_id,
		price
		)
	(
	select distinct 
		items.id, 
		dim_calendar.calendar_date,
	    dim_shop.id,
	    cast(staging.price as decimal(10,2))
	from staging
	left join
		items on
			md5(ROW(s10,s11,s12,s13,s14)::TEXT) 
			=
			md5(ROW(i10,i11,i12,i13,i14)::TEXT)		
	left join
		price_history.dim_calendar on
			cast(staging.calendar_date as date)
			=
			dim_calendar.calendar_date
	left join
		price_history.dim_shop on 
			staging.dealer_name
			=
			dim_shop.dealer_name
	where 
		(SELECT cast(CURRENT_TIMESTAMP AS DATE))
		= 
		cast(staging.calendar_date as date)
	)