
insert
	into
	price_history.smartphones_daily_aggregates (item_id,
	model_name,
	brand_name,
	shop_with_min_daily_price,
	date_scrapped,
	avg_daily_price,
	min_daily_price,
	max_daily_price
	)
select
	min(dia.smartphone_id), 
	di.item_name, 
	di.smartphone_brand, 
	(
	select
		dealer_name
	from
		price_history.items_facttable fck, price_history.dim_shop dss 
	where
		price = (select MIN(price) from price_history.items_facttable if3, price_history.dim_items di3 where if2.calendar_date = if3.calendar_date and di.item_name = di3.item_name and di3.smartphone_brand = di.smartphone_brand)
		and fck.shop_id = dss.shop_id 
	group by dealer_name 
	),
	cast(if2.calendar_date as Timestamp),
	avg(cast(if2.price as numeric)),
	min(cast(if2.price as numeric)),
	max(cast(if2.price as numeric))
from 
	price_history.dim_items_attributes dia , 
	price_history.dim_items di ,
	price_history.items_facttable if2,
	price_history.dim_shop ds
where 
	dia.smartphone_id = di.smartphone_id
	and  
	dia.smartphone_id = if2.smartphone_id
	and 
	if2.shop_id = ds.shop_id
group by
	(if2.calendar_date,
	di.item_name,
	di.smartphone_brand);



