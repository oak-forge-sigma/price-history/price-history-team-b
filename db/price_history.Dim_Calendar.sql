DROP TABLE IF exists price_history.Dim_Calendar;
create table price_history.Dim_Calendar(
	calendar_date date PRIMARY KEY,
    day_of_week varchar(10),
    day_of_month integer,
    week_of_year_iso integer,
    week_of_year_abs integer,
    calendar_month varchar(15),
    quarter_of_year integer, 
    calendar_year integer,
    weekday_indicator varchar(7)
);
