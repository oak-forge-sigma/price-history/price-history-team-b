DROP SCHEMA IF EXISTS price_history CASCADE ;
CREATE SCHEMA price_history;

DROP ROLE IF EXISTS price_history_group;
CREATE role price_history_user with login password '';
create role price_history_group;

grant connect on database postgres to price_history_group;
grant all on schema price_history to price_history_group;
