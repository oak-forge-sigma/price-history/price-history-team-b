INSERT INTO price_history.Dim_Calendar
    (calendar_date, day_of_week, day_of_month, week_of_year_iso,  week_of_year_abs, calendar_month, quarter_of_year,
    calendar_year, weekday_indicator)
SELECT
    day,
    rtrim(to_char(day, 'Day')),
    date_part('Day', day),
    date_part('W', day),
    to_char(day, 'WW')::int,
    rtrim(to_char(day, 'Month')),
    date_part('Quarter', day),
    date_part('year', day),
    CASE
        WHEN date_part('isodow', day) IN (6, 7) THEN 'Weekend'
        ELSE 'Weekday'
    END
FROM
    generate_series('2021-12-24'::date, '2022-12-31'::date, '1 day') day;