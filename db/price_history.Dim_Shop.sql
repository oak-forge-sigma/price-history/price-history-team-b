DROP TABLE IF exists price_history.Dim_Shop;
create table price_history.Dim_Shop(
	id serial primary key,
	dealer_name varchar(25) unique,
	full_dealer_name varchar (25) unique 
);