DROP TABLE IF exists price_history.Dim_Items_Attributes;
create table price_history.Dim_Items_Attributes(
	id serial primary key unique ,
	screen_size_inch varchar(50),
	screen_resolution_px varchar(50),
	battery_capacity varchar(50),
	operating_system_name varchar(50),
	operating_system_version  varchar(50),
	camera_back_full_scraped_txt varchar(50),
	camera_back_mpx varchar(50),
	camera_front_full_scraped_txt varchar(50),
	camera_front_mpx varchar(50)
);