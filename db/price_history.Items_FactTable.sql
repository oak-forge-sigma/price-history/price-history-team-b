DROP TABLE IF EXISTS price_history.Items_FactTable;
CREATE TABLE price_history.Items_FactTable(
	id serial primary key,
	id_items int,
	calendar_date date,
	shop_id int,
	price decimal(10,2)
);