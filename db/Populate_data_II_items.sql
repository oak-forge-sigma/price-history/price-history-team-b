with
	items_attributes as
		(
		select distinct 	
			screen_size_inch as ia1 ,
			screen_resolution_px as ia2 ,
			battery_capacity as ia3 ,
			operating_system_name as ia4,
			operating_system_version as ia5,
			camera_back_full_scraped_txt as ia6,
			camera_back_mpx as ia7,
			camera_front_full_scraped_txt as ia8,
			camera_front_mpx as ia9,
			id
		from price_history.dim_items_attributes
		),
	staging as 
		(
		select distinct	
			main_screen_size_inch as s1,
			main_screen_resolution_mpx as s2,
			battery_capacity as s3,
			operating_system_name as s4,
			operating_system_version as s5,
			camera_back_full_scraped_txt as s6,
			camera_back_mpx as s7,
			camera_front_full_scraped_txt as s8,
			camera_front_mpx as s9,
			item_brand as s10,
			item_model as s11,
			color as s12,
			ram_memory_gb as s13,
			built_in_memory_gb as s14,
			dealer_name,
			calendar_date
		from price_history.smartphones_details_daily_stg
		),
	items as 
		(
		select distinct
			id,
			item_brand as i10,
			item_model as i11,
			color as i12,
			ram_memory_gb as i13,
			built_in_memory_gb as i14,
			id_items_attributes
		from price_history.dim_items
		)
		
insert into 
	price_history.dim_items
		(
		item_brand,														
		item_model,			
		color,
		ram_memory_gb,
		built_in_memory_gb,
		id_items_attributes
		)
	select distinct 	
		s10, s11, s12, s13 ,s14, items_attributes.id	
	from staging
	
	left join
		items_attributes on
			md5(ROW(s1,s2,s3,s4,s5,s6,s7,s8,s9)::TEXT) 
			=
			md5(ROW(ia1,ia2,ia3,ia4,ia5,ia6,ia7,ia8,ia9)::TEXT)

	left join
		items on
			md5(ROW(s10,s11,s12,s13,s14)::TEXT) 
			=
			md5(ROW(i10,i11,i12,i13,i14)::TEXT)
		and 
			items_attributes.id
			=
			items.id_items_attributes		
	where
		items.id is null 
	and	
		staging.dealer_name  = 'rtv_euro_agd' 
	and
		(SELECT cast(CURRENT_TIMESTAMP AS DATE))
		=
		cast(staging.calendar_date as date);