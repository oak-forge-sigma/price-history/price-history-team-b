with
	items as
		(
		select
			id,
			item_brand as brand,
			item_model as model,
			color as color,
			ram_memory_gb as ram,
			built_in_memory_gb as memory,
			id_items_attributes as att_id 
		from price_history.dim_items
		),		
		
	facts as
		(
		select
			id,
			id_items,
			calendar_date,
			price
		from price_history.items_facttable	
		),
		
	calendar as
		(
		select
			calendar_date
		from price_history.dim_calendar
		),
				
	max_day as
		(
		select
			max(calendar_date) as last_day
		from price_history.items_facttable	
		)

	
select 	last_day_available.brand,
		last_day_available.model,
		last_day_available.color,
		last_day_available.ram,
		last_day_available.memory,
		last_day_available.att_id,
		last_day_available.last_day as "last_day_available"
		from
	(
	select 
		items.brand,
		items.model,
		items.color,
		items.ram,
		items.memory,
		items.att_id,
		max(calendar.calendar_date) as last_day
	FROM facts
	join items on items.id = facts.id_items
	join calendar  on facts.calendar_date = calendar.calendar_date 
	group by 
		items.brand,
		items.model,
		items.color,
		items.ram,
		items.memory,
		items.att_id
	
	) last_day_available
	
left join 

	( 
	select
		'' as newfield,
		last_day
	from max_day
	) last_day_in_DWH
	
	
on last_day_available.last_day = last_day_in_DWH.last_day
where last_day_in_DWH.newfield is null
order by last_day_available.last_day desc 	


	
	