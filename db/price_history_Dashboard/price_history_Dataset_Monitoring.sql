select distinct(ds.full_dealer_name) as BRAND , count(if2.id) as NumberOfProducts,dc.day_of_week
from price_history.items_facttable if2 
right join price_history.dim_items di on di.id = if2.id_items 
join price_history.dim_calendar dc on dc.calendar_date = if2.calendar_date and dc.calendar_date > now() -  interval'9 Days'
join price_history.dim_shop ds on ds.id = if2.shop_id  
group by ds.full_dealer_name,dc.day_of_week 
order by dc.day_of_week desc
;
