SELECT distinct(di.item_model) as Model,di.item_brand as Brand ,di.color as Color ,di.ram_memory_gb as RamMemory ,di.built_in_memory_gb as Memory ,ds.full_dealer_name as Shop,
	last_value(dc.week_of_year_iso) OVER w AS week,
       first_value(price) OVER w AS MinValue, 
       last_value(price) OVER w AS MaxValue,
       first_value(price) over w2 as CurrentPrice, 
       first_value(if2.calendar_date) over w as last_date_when_MIN,
       last_value(if2.calendar_date) over w as last_date_when_MAX
FROM price_history.items_facttable if2 
right join price_history.dim_items di on di.id = if2.id_items
join price_history.dim_calendar dc on if2.calendar_date = dc.calendar_date 
join price_history.dim_shop ds on if2.shop_id = ds.id 
WINDOW w AS (PARTITION BY di.item_model ORDER BY price
             RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING),
             w2 as (partition by di.item_model order by if2.calendar_date desc
             RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING),
             w_month as (partition by if2.id_items order by dc.calendar_month RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING),
             w_week as (partition by if2.id_items order by dc.week_of_year_iso RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
ORDER BY di.item_model;
