
with
	items as
		(
		select
			id,
			item_brand as brand,
			item_model as model,
			color as color,
			ram_memory_gb as ram,
			built_in_memory_gb as memory,
			id_items_attributes as att_id 
		from price_history.dim_items
		),
		
	shops as
		(
		select
			id,
			full_dealer_name as dealer
		from price_history.dim_shop		
		),
		
	facts as
		(
		select
			id,
			id_items,
			calendar_date,
			shop_id,
			price
		from price_history.items_facttable	
		),
		
	calendar as
		(
		select
			calendar_date
		from price_history.dim_calendar
		),
		
	items_attributes as
		(
		select 
			id 
		from price_history.dim_items_attributes
		),
	max_day as
		(
		select
			max(calendar_date) as last_day
		from price_history.items_facttable	
		)		
		
select
	month_prices.brand as "Brand",
	month_prices.model as "Model",
	month_prices.color as "Color",
	month_prices.ram as "Ram[Gb]",
	month_prices.memory as "Build in memory[Gb]",
	month_prices.att_id as "Other attributes id",
	round(month_prices.avg,2) as "Last month avg. price",
	month_prices.min as "Last month min. price",
	month_prices.max as "Last month max. price",
	daily_prices.min_price_today as "Last price(Today/Yesterday)",
	string_agg(distinct daily_prices.dealer, ' , ' order by daily_prices.dealer)  as "Best price dealer Store",
	case 
		when round((daily_prices.min_price_today - round(month_prices.avg,2))/daily_prices.min_price_today*100,0) < 0
		then - round((round(month_prices.avg,2) - daily_prices.min_price_today)/round(month_prices.avg,2)*100,0) 
		else round((daily_prices.min_price_today - round(month_prices.avg,2))/daily_prices.min_price_today*100,0) 
	end as " + Price increase / - Price Drop"
	
from
			
		
(
	select 
		items.brand,
		items.model,
		items.color,
		items.ram,
		items.memory,
		items.att_id,
		avg(facts.price),
		min(facts.price),
		max(facts.price)
	FROM facts
	join items on items.id = facts.id_items
	join calendar  on facts.calendar_date = calendar.calendar_date 
	join shops on facts.shop_id = shops.id
	join items_attributes on items_attributes.id = items.att_id
	where calendar.calendar_date between (select last_day from max_day ) -30 and (select last_day from max_day ) -1
	
	group by items.brand, items.model, items.color, items.ram, items.memory, items.att_id	
) month_prices

		
inner join		
				
		
(
	select
		t_min_price.brand,
		t_min_price.model,
		t_min_price.color,
		t_min_price.ram,
		t_min_price.memory,
		t_min_price.att_id,
		t_min_price.min_price_today,
		t_min_price_per_dealer.dealer
		
	from
		(	
		select 
				items.brand,
				items.model,
				items.color,
				items.ram,
				items.memory,
				items.att_id,
				min(facts.price) as min_price_today
			FROM facts
			join items on items.id = facts.id_items
			join max_day  on facts.calendar_date = max_day.last_day
			join shops on facts.shop_id = shops.id
			join items_attributes on items_attributes.id = items.att_id
			group by items.brand, items.model, items.color, items.ram, items.memory, items.att_id
		) t_min_price		
				
		left join			
						
		(	
			select  
				items.brand,
				items.model,
				items.color,
				items.ram,
				items.memory,
				items.att_id,
				min(facts.price) as min_price_today,
				shops.dealer
			FROM facts
			join items on items.id = facts.id_items
			join max_day  on facts.calendar_date = max_day.last_day
			join shops on facts.shop_id = shops.id
			join items_attributes on items_attributes.id = items.att_id
			group by items.brand, items.model, items.color, items.ram, items.memory, items.att_id, shops.dealer
		) t_min_price_per_dealer
					
	on 
		t_min_price.brand = t_min_price_per_dealer.brand and
		t_min_price.model = t_min_price_per_dealer.model and
		t_min_price.color = t_min_price_per_dealer.color and
		t_min_price.ram = t_min_price_per_dealer.ram and
		t_min_price.memory = t_min_price_per_dealer.memory and
		t_min_price.att_id = t_min_price_per_dealer.att_id and 
		t_min_price.min_price_today = t_min_price_per_dealer.min_price_today
) daily_prices

on 
	month_prices.brand = daily_prices.brand and
	month_prices.model = daily_prices.model and
	month_prices.color = daily_prices.color and
	month_prices.ram = daily_prices.ram and
	month_prices.memory = daily_prices.memory and
	month_prices.att_id = daily_prices.att_id
	
group by 
	month_prices.brand,
	month_prices.model,
	month_prices.color,
	month_prices.ram,
	month_prices.memory,
	month_prices.att_id,
	daily_prices.min_price_today,
	"Last month avg. price",
	"Last month min. price",
	"Last month max. price"	
	
