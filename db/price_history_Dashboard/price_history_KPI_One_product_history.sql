

with
	items as
		(
		select
			id,
			item_brand as brand,
			item_model as model,
			color as color,
			ram_memory_gb as ram,
			built_in_memory_gb as memory,
			id_items_attributes as att_id 
		from price_history.dim_items
		),
		
	shops as
		(
		select
			id,
			full_dealer_name as dealer
		from price_history.dim_shop		
		),
		
	facts as
		(
		select
			id,
			id_items,
			calendar_date,
			shop_id,
			price
		from price_history.items_facttable	
		),
		
	calendar as
		(
		select
			calendar_date
		from price_history.dim_calendar
		),
		
	items_attributes as
		(
		select 
			id 
		from price_history.dim_items_attributes
		)
		
		
	select 
		items.brand,
		items.model,
		items.color,
		items.ram,
		items.memory,
		items.att_id,
		facts.price,
		calendar.calendar_date,
		shops.dealer
	FROM facts
	join items on items.id = facts.id_items
	join calendar  on facts.calendar_date = calendar.calendar_date 
	join shops on facts.shop_id = shops.id
	join items_attributes on items_attributes.id = items.att_id


		