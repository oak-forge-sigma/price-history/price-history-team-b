select if2.item_id,item_attributes ,dia.item_brand,dia.item_model,dia.color ,dia.battery_capacity , dia.ram_memory_mb , dia.built_in_memory_gb , dia.camera_back_mpx ,if2.price,dc.week_of_year_iso , if2.calendar_date,dc.day_of_week,
((if2.price /LAG(if2.price) over(order by dc.week_of_year_iso))) as percent_change_to_last_week
from price_history.items_facttable if2 
join price_history.dim_items_attributes dia on dia.item_id = if2.item_attributes 
join price_history.dim_calendar dc on dc.calendar_date = if2.calendar_date 
order by dc.week_of_year_iso , if2.calendar_date desc;