DROP TABLE IF exists price_history.Dim_Items;
create table price_history.Dim_Items(
	id serial primary key unique,
	item_brand varchar(50) not null,
	item_model varchar(100) not null,
	color varchar(50),
	ram_memory_gb varchar(50),
	built_in_memory_gb varchar(50),
	id_items_attributes numeric
);