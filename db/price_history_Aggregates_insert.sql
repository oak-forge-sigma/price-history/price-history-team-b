with
	items as
		(
		select
			id,
			item_brand as brand,
			item_model as model,
			color as color,
			ram_memory_gb as ram,
			built_in_memory_gb as memory,
			id_items_attributes as att_id 
		from price_history.dim_items
		),
		
	shops as
		(
		select
			id,
			full_dealer_name as dealer
		from price_history.dim_shop		
		),
		
	facts as
		(
		select
			id,
			id_items,
			calendar_date,
			shop_id,
			price
		from price_history.items_facttable	
		),
		
	calendar as
		(
		select
			calendar_date,
			calendar_year,
			week_of_year_iso
		from price_history.dim_calendar
		),
		
	items_attributes as
		(
		select 
			id 
		from price_history.dim_items_attributes
		)
insert into price_history.smartphones_weekly_aggregates (
	"Year",
	"Week of year",
	"Time interval",
	"Brand",
	"Model",
	"Color",
	"Ram",
	"Memory",
	"ID others att",
	"Average last week price",
	"Minimum  last week price",
	"Maximum last week price",
	"The Lowest price Store "
	)		
		
	select
	tabela2.calendar_year as "Year",
	tabela2.week_of_year_iso as "Week of year",
	tabela1.Time_interval as "Time interval",
	tabela1.brand as "Brand",
	tabela1.model as "Model",
	tabela1.color as "Color",
	tabela1.ram as "Ram",
	tabela1.memory as "Memory",
	tabela1.att_id as "ID others att",
	round(tabela1.avg,2) as "Average last week price",
	tabela1.min as "Minimum  last week price",
	tabela1.max as "Maximum last week price",
	string_agg(distinct tabela2.dealer, ' , ' order by tabela2.dealer) as "Store with the lowest price"
from
		(
		select
			items.brand,
			items.model,
			items.color,
			items.ram,
			items.memory,
			items.att_id,
			avg(facts.price),
			min(facts.price),
			max(facts.price),
			concat(min(calendar.calendar_date),' - ', max(calendar.calendar_date)) as Time_interval
		FROM facts
		join items on items.id = facts.id_items
		join calendar  on facts.calendar_date = calendar.calendar_date 
		join shops on facts.shop_id = shops.id
		join items_attributes on items_attributes.id = items.att_id
		
		where calendar.week_of_year_iso =extract(week from now()) -1
		group by 1,2,3,4,5,6
		) tabela1

	left join 
	
		(
		select
			items.brand,
			items.model,
			items.color,
			items.ram,
			items.memory,
			items.att_id,
			min(facts.price) as dealers_min,
			shops.dealer,
			calendar.week_of_year_iso,
			calendar.calendar_year 

		FROM facts
		join items on items.id = facts.id_items
		join calendar  on facts.calendar_date = calendar.calendar_date 
		join shops on facts.shop_id = shops.id
		join items_attributes on items_attributes.id = items.att_id
		where calendar.week_of_year_iso =extract(week from now()) -1
		group by 1,2,3,4,5,6,8,9,10
		) tabela2
	on tabela1.model = tabela2.model and tabela1.min = tabela2.dealers_min and  tabela1.brand  = tabela2.brand and tabela1.color = tabela2.color and tabela1.ram = tabela2.ram and tabela1.memory = tabela2.memory and tabela1.att_id = tabela2.att_id
group by 1,2,3,4,5,6,7,8,9,10,11,12
;