with
	items_attributes as
		(
		select distinct 	
			screen_size_inch as ia1 ,
			screen_resolution_px as ia2 ,
			battery_capacity as ia3 ,
			operating_system_name as ia4,
			operating_system_version as ia5,
			camera_back_full_scraped_txt as ia6,
			camera_back_mpx as ia7,
			camera_front_full_scraped_txt as ia8,
			camera_front_mpx as ia9,
			id
		from price_history.dim_items_attributes
		),
	staging as 
		(
		select distinct	
			main_screen_size_inch as s1,
			main_screen_resolution_mpx as s2,
			battery_capacity as s3,
			operating_system_name as s4,
			operating_system_version as s5,
			camera_back_full_scraped_txt as s6,
			camera_back_mpx as s7,
			camera_front_full_scraped_txt as s8,
			camera_front_mpx as s9,
			dealer_name,
			calendar_date 
		from price_history.smartphones_details_daily_stg
		)
insert into
	price_history.dim_items_attributes
		(
		screen_size_inch ,
		screen_resolution_px ,
		battery_capacity ,
		operating_system_name,
		operating_system_version,
		camera_back_full_scraped_txt,
		camera_back_mpx,
		camera_front_full_scraped_txt,
		camera_front_mpx
		)
		
	select distinct
		s1,s2,s3,s4,s5,s6,s7,s8,s9
	from staging
	
	left join
		items_attributes on
			md5(ROW(ia1,ia2,ia3,ia4,ia5,ia6,ia7,ia8,ia9)::TEXT)
			=
			md5(ROW(s1,s2,s3,s4,s5,s6,s7,s8,s9)::TEXT) 
	where
		items_attributes.id is null 
	and 
		staging.dealer_name = 'rtv_euro_agd'
	and
		(SELECT cast(CURRENT_TIMESTAMP AS DATE))
		=
		cast(staging.calendar_date as DATE);